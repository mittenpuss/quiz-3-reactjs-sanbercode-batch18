import React,{useContext, useEffect} from 'react'
import { Redirect } from 'react-router-dom'
import { LoginContext } from '../context'
import SanberLogo from "../images/logo.png"

const NavBar = () =>{

    const [inputLogin,setInputLogin] = useContext(LoginContext)

    var sudahLogin = localStorage.getItem('isLogin');

    const onClickLogout=()=>{
        setInputLogin({
            username: 'admin',
            password: 'admin',
            isLogin:false
        })
        localStorage.removeItem('isLogin')
    }

    return (
        <header style={{position:'sticky',top:0}}>
            <div style={{display:'flex', flexDirection:'row',justifyContent:'space-between', backgroundColor:'white',width:'100%',height:'50px'}}>
                <div>
                    <img id='logo' src={SanberLogo} width='200px' alt='logo_sanbercode'/>
                </div>
                <div style={{alignSelf:'center'}}>
                    <a href="/" style={{paddingRight:'20px'}}>Home</a>
                    <a href="/about" style={{paddingRight:'20px'}}>About</a>
                    <a href="/contact" style={{paddingRight:'20px'}}>Contact</a>
                    
                    {
                    sudahLogin?
                    <a href="/movieEditor" style={{paddingRight:'20px'}}>Movie Editor</a>
                    :
                    null
                    }
                    
                    {
                    sudahLogin?
                        <a href="/" style={{paddingRight:'20px' }} onClick={onClickLogout}>Logout</a>
                        :
                        <a href="/login" style={{paddingRight:'20px'}}>Login</a>
                    }

                </div>
            </div>
        </header>
    )
}

export default NavBar