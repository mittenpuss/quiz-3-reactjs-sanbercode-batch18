import React from 'react'

const Footer = () =>{
    return (
        <footer style={{bottom:0}}>
            <h5>copyright &copy; 2020 by Sanbercode</h5>
        </footer>
        )
}

export default Footer