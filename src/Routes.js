import React from "react";
import { Switch, Route } from "react-router";
import Index from "./pages";
import About from "./pages/about";
import Contact from "./pages/contact";
import Login from "./pages/login";
import MovieEditor from "./pages/movieEditor";


const Routes = () => {

  return (
    <Switch>
      <Route exact path="/">
        <Index/>
      </Route>
      <Route path="/about">
        <About/>
      </Route>
      <Route exact path="/contact">
        <Contact/>
      </Route>
      <Route exact path="/login">
        <Login/>
      </Route>
      <Route exact path="/movieEditor">
        <MovieEditor/>
      </Route>
    </Switch>
  );
};

export default Routes;