import React from 'react';
import './App.css';
import { BrowserRouter as Router } from "react-router-dom";
import Routes from './Routes';
import NavBar from './components/NavBar';
import { LoginProvider } from './context';

function App() {


  return (
    <LoginProvider>
        <Router>
        <div className="App">
          <NavBar/>
          <Routes/>
        </div>
        </Router>   
    </LoginProvider>

  );
}

export default App;
