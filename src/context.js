import React, { createContext, useState } from "react";

export const LoginContext = createContext();

export const LoginProvider = (props) => {

    const [inputLogin,setInputLogin] = useState({
        username: 'admin',
        password: 'admin',
        isLogin: false
    });

    return (
        <LoginContext.Provider value={[inputLogin, setInputLogin]}>
            {props.children}
        </LoginContext.Provider>
    );
};

