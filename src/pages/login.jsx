import React, { useContext, useState } from 'react';
import './styles.css'
import {Redirect} from 'react-router-dom'
import { LoginContext } from '../context';

const Login = () =>{


    const [inputLogin,setInputLogin] = useContext(LoginContext)
    const [inputUsername,setInputUsername] = useState('')
    const [inputPassword,setInputPassword] = useState('')

    const onClickLogin=()=>{
        if(inputUsername===inputLogin.username&&inputPassword===inputLogin.password){
            setInputLogin({
                username: 'admin',
                password: 'admin',
                isLogin:true
            })
            localStorage.setItem('isLogin', true);
        }else{
            alert('Password dan Username tidak sesuai')
        }
    }

    //kalau sudah login tidak bisa masuk
    var sudahLogin = localStorage.getItem('isLogin');
    if(sudahLogin){
        return <Redirect to={'/'}></Redirect>
    }

    return (

        <div id='containerIndex' style={{minHeight:'100vh',textAlign:'center'}}>
                <div>
                    <input style={{marginTop:'100px'}} type='text' placeholder='Username' onChange={e => setInputUsername(e.target.value)}></input>
                </div>
                <div>
                    <input type='password' placeholder='Password' onChange={e => setInputPassword(e.target.value)}></input>
                </div>
                <div>
                    <button style={{marginTop:'20px'}} onClick={onClickLogin}>
                        LOGIN
                    </button>
            
                </div>
         </div>
    )
}

export default Login