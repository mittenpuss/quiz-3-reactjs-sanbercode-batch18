import React from 'react'

const Contact = () =>{
    return (
    <div style={{backgroundColor:'white', paddingTop:'20px', marginLeft:'100px'}}>
        <h1 style={{textAlign:'center'}}>Hubungi Kami</h1>
        <div style={{paddingLeft:'10px'}}>
            <p><b>Kantor: </b>Jalan Belum Jadi</p>
            <p><b>Nomor Telepon: </b>08XX-XXX-XXX-XXX</p>
            <p><b>Email: </b>email@belumjadi.com</p>
        </div>
        <h1 style={{textAlign:'center'}}>Kirimkan Pesan</h1>

        <div style={{paddingLeft:'10px',display: 'flex', flexDirection:'column', width:"40%"}}>
            <div style={{display:'flex', flexDirection:'row'}}>
                <div style={{flex:'1'}}>
                    <span>Nama </span>
                </div>
                <div style={{flex:'2'}}>
                    <input type="text" size={20}/>
                </div>
            </div>

        <div style={{display:'flex', flexDirection:'row', marginTop:'10px'}}>
            <div style={{flex:'1'}}>
                <span>Email </span>
            </div>
            <div style={{flex:'2'}}>
                <input type="text" size={20} />
            </div>
        </div>

        <div style={{display:'flex', flexDirection:'row', marginTop:'10px'}}>
            <div style={{flex:'1'}}>
                <span>Jenis Kelamin </span>
            </div>
            <div style={{flex:'2'}}>
                    <input type="radio" id="male" name="gender" value="male"/>
                    <label for="male">Laki-laki</label><br/>
                    <input type="radio" id="female" name="gender" value="female"/>
                    <label for="female">Perempuan</label><br/>
            </div>
        </div>

        <div style={{display:'flex', flexDirection:'row', marginTop:'10px'}}>
            <div style={{flex:'1'}}>
                <span>Pesan Anda </span>
            </div>
            <div style={{flex:'2'}}>
                <textarea name="" id="" cols="50" rows="5"></textarea>
            </div>
        </div>
        <button style={{width:'50px'}}>Kirim</button>        
        </div>

    </div>
        
    )
}

export default Contact


{/* <div style={{backgroundColor:'white', paddingTop:'20px'}}>
            <h1 style={{textAlign:'center'}}>Hubungi Kami</h1>
            <div style={{paddingLeft:'10px'}}>
                <p><b>Kantor: </b>Jalan Belum Jadi</p>
                <p><b>Nomor Telepon: </b>08XX-XXX-XXX-XXX</p>
                <p><b>Email: </b>email@belumjadi.com</p>
            </div>
            <h1 style={{textAlign:'center'}}>Kirimkan Pesan</h1>
       
         <div style={{paddingLeft:'10px',display: 'flex', flexDirection:'column', width:"40%"}}>
            <div style={{display:'flex', flexDirection:'row'}}>
                <div style={{flex:1}}>
                    <span>Nama </span>
                </div>
                <div style="flex: 2;">
                    <input type="text" size="20" >
                </div>
            </div>
            <div style="display: flex; flex-direction: row; margin-top: 10px;">
                <div style="flex: 1;">
                    <span>Email </span>
                </div>
                <div style="flex: 2;">
                    <input type="text" size="20" >
                </div>
            </div>
            <div style="display: flex; flex-direction: row; margin-top: 10px;">
                <div style="flex: 1;">
                    <span>Jenis Kelamin </span>
                </div>
                <div style="flex: 2;">
                        <input type="radio" id="male" name="gender" value="male">
                        <label for="male">Laki-laki</label><br>
                        <input type="radio" id="female" name="gender" value="female">
                        <label for="female">Perempuan</label><br>
                </div>
            </div>
            <div style="display: flex; flex-direction: row; margin-top: 10px;">
                <div style="flex: 1;">
                    <span>Pesan Anda </span>
                </div>
                <div style="flex: 2;">
                    <textarea name="" id="" cols="50" rows="5"></textarea>
                </div>
            </div>
            <button style="width: 50px;">Kirim</button>        
            </div> */}