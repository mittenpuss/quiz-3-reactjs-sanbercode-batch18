import React,{useEffect,useState} from 'react'
import './styles.css'
import axios from 'axios'

const Index  = () =>{

    const [movieList, setMovieList] =  useState(null)

    useEffect(() => {
        axios.get('http://backendexample.sanbercloud.com/api/movies')
        .then((res)=>{
            console.log(res.data)
            setMovieList(res.data)
        })
        .catch((err)=>{
            console.log(err)
        })
      },[]);
              
    

    return (
        <div id='containerIndex'>
            <div style={{paddingTop:'5px'}}/>
            <div style={{backgroundColor:'white', marginLeft:'10%',marginRight:'10%'}}>
                <h1 style={{paddingTop:'20px',textAlign:'center'}}>Daftar Film Terbaik</h1>

                {
                movieList && movieList.map((item,index)=>{

                    var num = item.duration;
                    var hours = (num / 60);
                    var rhours = Math.floor(hours);
                    var minutes = (hours - rhours) * 60;
                    var rminutes = Math.round(minutes);
                    var total =  rhours + " jam " + rminutes + " menit";

                        return (
                            <div key={index} style={{marginLeft:'40px',marginBottom:'40px'}}>
                                <h2 style={{textDecoration:'underline'}}>{item.title}</h2>                                
                                <div style={{display:'flex',flexDirection:'row'}}>
                                    <div>
                                        <img src={item.image_url} alt='gambar_film' width='550px' height='400px' style={{objectFit:'cover'}}></img>
                                    </div>
                                    <div style={{marginLeft:'30px'}}>
                                        <h3><strong>Rating: </strong>{item.rating} </h3>
                                        <h3><strong>Durasi: </strong>{total} </h3>
                                        <h3><strong>Genre: </strong>{item.genre}</h3>
                                    </div>
                                </div>
                                <div style={{paddingRight:'30px',paddingBottom:'30px'}}>
                                    <p><strong>Description:</strong> {item.description}</p>
                                </div>
                            </div>

                        )
                })}
                
            </div>
            <div style={{paddingBottom:'10px'}}/>
        </div>
    )
}

export default Index