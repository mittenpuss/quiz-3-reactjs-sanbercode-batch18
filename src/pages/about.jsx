
import React from 'react'

class About extends React.Component{
    render() {
        return (
        <div>
            <div style={{padding:'10px', border:'1px solid #ccc'}}>
                <h1 style={{textAlign:'center'}}>Data Peserta Sanbercode Bootcamp Reactjs</h1>
                <div style={{textAlign:'center'}}>
                    <p><strong>Nama:</strong> Dewa Gede Angga Wijaya</p>
                    <p><strong>Email:</strong> anggaa_wijaya@yahoo.com</p>
                    <p><strong>Sistem Operasi yang digunakan:</strong> Windows</p>
                    <p><strong>Akun Gitlab:</strong> <a href="https://gitlab.com/mittenpuss">gitlab.com/mittenpuss</a></p>
                    <p><strong>Akun Telegram:</strong> Angga Wijaya</p>
                </div>
            </div>
        </div>
        )
    }
}

export default About