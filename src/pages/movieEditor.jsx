import React,{useState,useEffect,useContext} from 'react';
import axios from 'axios'
import "./styles.css"
import { LoginContext } from '../context'
import { Redirect } from 'react-router-dom';

const MovieEditor = () =>{
    
    const [inputLogin,setInputLogin] = useContext(LoginContext)
    const [movieList, setMovieList] =  useState(null)
    const [inputTitle,setInputTitle] = useState('')
    const [inputDescription,setInputDescription] = useState('')
    const [inputYear,setInputYear] = useState(2020)
    const [inputDuration,setInputDuration] = useState(120)
    const [inputGenre,setInputGenre] = useState('')
    const [inputRating,setInputRating] = useState(0)
    const [inputImageURL,setInputImageURL] = useState('')
    const [editID,setEditID] = useState(0)

    useEffect(() => {
        axios.get('http://backendexample.sanbercloud.com/api/movies')
        .then((res)=>{
            console.log(res.data)
            setMovieList(res.data)
        })
        .catch((err)=>{
            console.log(err)
        })
      },[]);


    //kalau belum login tidak bisa masuk
    var sudahLogin = localStorage.getItem('isLogin');
    if(!sudahLogin){
        return <Redirect to={'/'}></Redirect>
    }


    const onDeleteClick=(id)=>{
        axios.delete(`http://backendexample.sanbercloud.com/api/movies/${id}`)
        .then((res)=>{
            axios.get('http://backendexample.sanbercloud.com/api/movies')
            .then((res)=>{
                setMovieList(res.data)
                setInputTitle('')
                setInputDescription('')
                setInputYear(2020)
                setInputDuration(120)
                setInputGenre('')
                setInputRating(0)
            })
            .catch((err)=>{
            })
        })
        .catch((err)=>{
        })
    }

    const onEditClick=(id,title,description,year,duration,genre,rating,imageURL)=>{
        setEditID(id)
        setInputTitle(title)
        setInputDescription(description)
        setInputYear(year)
        setInputDuration(duration)
        setInputGenre(genre)
        setInputRating(rating)
        setInputImageURL(imageURL)
    }

    const onSubmitClick=()=>{
        if(editID){
            axios.put(`http://backendexample.sanbercloud.com/api/movies/${editID}`, {
                title: inputTitle,
                description: inputDescription,
                year: inputYear,
                duration: inputDuration,
                genre: inputGenre,
                rating: inputRating,
                image_url: inputImageURL
            })
            .then((res)=>{
                console.log(res)
                setEditID(0)
                setInputTitle('')
                setInputDescription('')
                setInputYear(2020)
                setInputDuration(120)
                setInputGenre('')
                setInputRating(0)
                setInputImageURL('')
                axios.get('http://backendexample.sanbercloud.com/api/movies')
                .then((res)=>{
                    setMovieList(res.data)
                })
                .catch((err)=>{
                    
                })

            })
            .catch((err)=>{
                console.log(err)
            })
        }else{

            console.log(inputTitle)
            console.log(inputDescription)
            console.log(inputYear)
            console.log(inputDuration)
            console.log(inputGenre)
            console.log(inputRating)
            console.log(inputImageURL)
            
            axios.post(`http://backendexample.sanbercloud.com/api/movies`, {
                title: inputTitle,
                description: inputDescription,
                year: inputYear,
                duration: inputDuration,
                genre: inputGenre,
                rating: inputRating,
                image_url: inputImageURL
            })
          .then(res => {
            console.log(res)
            axios.get('http://backendexample.sanbercloud.com/api/movies')
            .then((res)=>{
                setMovieList(res.data)
                setInputTitle('')
                setInputDescription('')
                setInputYear(2020)
                setInputDuration(120)
                setInputGenre('')
                setInputRating(0)
                setInputImageURL('')
            })
            .catch((err)=>{
                
            })
          })
          .catch((err)=>{
              console.log(err)
          })
        }
        }

    return (
        <div>
            <h1 style={{textAlign:'center'}}>Movies Form</h1>
            <div>
                <table style={{marginLeft:'auto',marginRight:'auto'}}>
                <thead>
                    <tr style={{backgroundColor:'darkgray'}}>
                        <th>No</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Year</th>
                        <th>Duration</th>
                        <th>Genre</th>
                        <th>Rating</th>
                        <th>Action</th>
                    </tr>
                </thead>

                {
                movieList && movieList.map((item,index)=>{
                    return (
                        <tbody key={index}>
                        <tr style={{backgroundColor:'coral'}}>
                            <td>{index+1}</td>
                            <td>{item.title}</td>
                            <td width={400}>{item.description}</td>
                            <td width={50}>{item.year}</td>
                            <td>{item.duration}</td>
                            <td width={150}>{item.genre}</td>
                            <td>{item.rating}</td>
                            
                            <td style={{textAlign:'center', display:'flex', flexDirection:'row',justifyContent:'center'}}>
                                <div style={{alignItems:'center'}}>
                                    <button onClick={()=>onEditClick(item.id,item.title,item.description,item.year,item.duration,item.genre,item.rating,item.image_url)}>
                                        Edit
                                    </button>
                                    <button onClick={()=>onDeleteClick(item.id)}>
                                        Delete
                                    </button>
                                </div>
                            </td>
                        </tr>  
                        </tbody>                              
                    )
                })
                }
                </table>
            </div>

            <div style={{borderWidth:'1px', borderColor:'black', borderStyle:'solid', marginTop:'50px', marginLeft:'20%', marginRight:'20%', borderRadius:'15px'}}>
                <div style={{textAlign:'center'}}>
                    <h1>Movies Form</h1>
                </div>
                
                <div style={{display:'flex', flexDirection:'row', width:'100%', paddingBottom:'15px'}}>
                    <div style={{paddingLeft:'20px',width:'30%'}}>
                        Title:
                    </div>
                    <div style={{width:'70%'}}>              
                        <input type="text" size={50} value={inputTitle} defaultValue={inputTitle} onChange={e => setInputTitle(e.target.value)}/>
                    </div>
                </div>

                <div style={{display:'flex', flexDirection:'row', width:'100%', paddingBottom:'15px'}}>
                    <div style={{paddingLeft:'20px',width:'30%'}}>
                        Description:
                    </div>
                    <div style={{width:'70%'}}>  
                        <textarea value={inputDescription} defaultValue={inputDescription} rows="4" cols="70" onChange={e => setInputDescription(e.target.value)} />            
                    </div>
                </div>

                <div style={{display:'flex', flexDirection:'row', width:'100%', paddingBottom:'15px'}}>
                    <div style={{paddingLeft:'20px',width:'30%'}}>
                        Year:
                    </div>
                    <div style={{width:'70%'}}>              
                        <input type="number" size={50} min={1980} value={inputYear} defaultValue={inputYear} onChange={e => setInputYear(e.target.value)}/>
                    </div>
                </div>

                <div style={{display:'flex', flexDirection:'row', width:'100%', paddingBottom:'15px'}}>
                    <div style={{paddingLeft:'20px',width:'30%'}}>
                        Duration:
                    </div>
                    <div style={{width:'70%'}}>              
                        <input type="number" name="name" value={inputDuration} defaultValue={inputDuration} onChange={e => setInputDuration(e.target.value)}/>
                    </div>
                </div>

                <div style={{display:'flex', flexDirection:'row', width:'100%', paddingBottom:'15px'}}>
                    <div style={{paddingLeft:'20px',width:'30%'}}>
                        Genre:
                    </div>
                    <div style={{width:'70%'}}>              
                        <input type="text" size={50} value={inputGenre} defaultValue={inputGenre} onChange={e => setInputGenre(e.target.value)}/>
                    </div>
                </div>

                <div style={{display:'flex', flexDirection:'row', width:'100%', paddingBottom:'15px'}}>
                    <div style={{paddingLeft:'20px',width:'30%'}}>
                        Rating:
                    </div>
                    <div style={{width:'70%'}}>              
                        <input type="number" name="name" max={10} min={0} value={inputRating>10?setInputRating(10):inputRating} defaultValue={inputRating} onChange={e => setInputRating(e.target.value)}/>
                    </div>
                </div>

                <div style={{display:'flex', flexDirection:'row', width:'100%', paddingBottom:'15px'}}>
                    <div style={{paddingLeft:'20px',width:'30%'}}>
                        Image URL:
                    </div>
                    <div style={{width:'70%'}}>              
                        <input type="text" size={50} value={inputImageURL} defaultValue={inputImageURL} onChange={e => setInputImageURL(e.target.value)}/>
                    </div>
                </div>
                
                <button onClick={()=> onSubmitClick()} style={{marginLeft:'20px',borderRadius:'20px', marginBottom:'30px'}}>
                    Kirim
                </button>
            
            </div>
        </div>
    )
}

export default MovieEditor